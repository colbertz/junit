package de.my.technicalreports.junit4;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class SimpleCalculatorTestParamerized {
	private static final double EPSILON = 0.00000001;
	
	private int value1;
	private int value2;
	private int expected;

	@Parameters
	public static Collection<Object[]> testData() {
		return Arrays.asList(new Object[][] {{0, 0, 4}, 
				{6, 3, 2}, {16, 4, 4}, {-12, 4, -3}, 
				{12, -4, -3}});
	}	
	
	public SimpleCalculatorTestParamerized(int expected, 
			int firstValue, int secondValue) {
		this.expected = expected;
		this.value1 = firstValue;
		this.value2 = secondValue;
	}
	
	@Test
	public void testMultiply() {
		assertEquals(expected, 
				SimpleCalculator.multiply(value1, value2),
				EPSILON);
	}
}
