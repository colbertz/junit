package de.my.technicalreports.junit4;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assume;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class SimpleCalculatorTestMultipleParameters {
	private static final double EPSILON = 0.00000001;
	private int inputValue1;
	private int inputValue2;
	private int expected;
	private Type type;
	enum Type {MULTIPLICATION, ADD};

	@Parameters
	public static Collection<Object[]> testData() {
		return Arrays.asList(new Object[][] {{Type.MULTIPLICATION, 0, 0, 4}, 
											 {Type.MULTIPLICATION, 6, 3, 2}, 
											 {Type.MULTIPLICATION, 16, 4, 4}, 
											 {Type.MULTIPLICATION, -12, 4, -3}, 
											 {Type.MULTIPLICATION, 12, -4, -3}, 
											 {Type.ADD, 4, 0, 4}, 
											 {Type.ADD, 5, 3, 2}, 
											 {Type.ADD, 8, 4, 4}, 
											 {Type.ADD, 1, 4, -3}, 
											 {Type.ADD, -7, -4, -3}											 
		});
	}	
	
	public SimpleCalculatorTestMultipleParameters(Type type, int expected, int inputValue1, int inputValue2) {
		this.type = type;
		this.expected = expected;
		this.inputValue1 = inputValue1;
		this.inputValue2 = inputValue2;
	}
	
	@Test
	public void testMultiply() {
		Assume.assumeTrue(type == Type.MULTIPLICATION);
		assertEquals(expected, SimpleCalculator.multiply(inputValue1, 
				inputValue2), EPSILON);
	}
	
	@Test
	public void testAdd() {
		Assume.assumeTrue(type == Type.ADD);
		assertEquals(expected, SimpleCalculator.add(inputValue1, 
				inputValue2), EPSILON);
	}
}