package de.my.technicalreports.junit4;

import static org.junit.Assert.*;

import org.junit.Test;

public class SimpleCalculatorTest {
	private static final double EPSILON = 0.00000001;
	
	@Test
	public void testAdd() {
		double result = SimpleCalculator.add(4, 5);
		assertEquals(9, result, EPSILON);
	}

	@Test
	public void testSubtract() {
		double result = SimpleCalculator.subtract(4, 5);
		assertEquals(-1, result, EPSILON);
	}
	
	@Test
	public void testMultiply() {
		double result = SimpleCalculator.multiply(4, 5);
		assertEquals(20, result, EPSILON);
	}
	
	@Test
	public void testDivide() {
		double result = SimpleCalculator.divide(4, 2);
		assertEquals(2, result, EPSILON);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testDivideByZero1() {
		SimpleCalculator.divide(4, 0);
	}
	
	@Test
	public void testDivideByZero2() {
		try {
			SimpleCalculator.divide(4, 0);
			fail();
		} catch (IllegalArgumentException ex) {
			String message = ex.getMessage();
			assertEquals(message, "Division durch 0 ist "
					+ "nicht erlaubt! Bitte wechseln Sie "
					+ "dafür das Universum");
		}
	}
}
