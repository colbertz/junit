package de.my.technicalreports.junit4;

public class SimpleCalculator {
	public static double add(double value1, double value2) {
		return value1 + value2;
	}
	
	public static double subtract(double value1, double value2) {
		return value1 - value2;
	}
	
	public static double divide(double value1, double value2) {
		if (value2 != 0) {
			return value1 / value2;
		} else {
			throw new IllegalArgumentException("Division durch 0 ist nicht erlaubt! "
					+ "Bitte wechseln Sie dafür das Universum");
		}
	}
	
	public static double multiply(double value1, double value2) {
		return value1 * value2;
	}
}
