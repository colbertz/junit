package de.my.technicalreports.junit4;

import java.util.List;

public class DatabaseSimulation {
	private List<String> database;
	
	public DatabaseSimulation(List<String> strings) {
		database = strings;
	}
	
	public void addString(String aString) {
		database.add(aString);
	}
	
	public void deleteString(String aString) {
		database.remove(aString);
	}
	
	public int size() {
		return database.size();
	}
	
	public String getString(int index) {
		return database.get(index);
	}
	
	public void complexAlgorithm() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
